<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vero extends Model
{
    use HasFactory;
    public $table = "tb_vero";
    protected $fillable = ['asin','judul','brand','keyword','penginput'];
}
