<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Seller extends Model
{
    use HasFactory;
    public $table = "seller";
    protected $fillable = ['seller_ebay','penginput_seller','status_seller','ket_seller'];
}
