<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class product extends Model
{
    use HasFactory;
    public $table = "tb_product";
    protected $fillable = ['asin','judul','harga','brand','image','judul_eb','harga_eb','account_ebay','penginput','penginput_id'];
   public function user()
    {
        return $this->belongsTo(User::class, 'penginput_id');
    }
    
    
}
