<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
   
    public function SuperadminHome()
    {
        return view('home',["msg"=>"Hello! I am"]);
    }
    public function Home()
    {
        return view('home',["msg"=>"Hello! I am"]);
    }
    // public function listerHome()
    // {
    //     return view('home',["msg"=>"Hello! I am"]);
    // }
    // public function risetHome()
    // {
    //     return view('home',["msg"=>"Hello! I am"]);
    // }
    
}
