<?php

namespace App\Http\Controllers;

use App\Imports\VeroImportClass;
use App\Models\Vero;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\DataTables;

class VeroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data_vero = Vero::all();
            return Datatables::of($data_vero)
            ->addIndexColumn()
            ->addColumn('date', function ($data) {
                return date('d-M-Y h:i A', strtotime($data->updated_at));
            })
            ->addColumn('action', function ($data) {
                return '<a href="https://amazon.com/dp/' . $data->asin . '" target="_blank">'.$data->asin.'</a>';
            })
           ->addColumn('aksi', function ($data) {
                return "<a href='#' data-id='$data->id' class='delete btn btn-danger btn-icon-split tombol-del'>
                            <span class='icon text-white-5'>
                              <i class='fas fa-trash'></i>
                            </span>
                            <span class='text'>Hapus</span>
                        </a>";
            })
            ->rawColumns(['checkbox','aksi','date','action'])
            ->make(true);
        }
        return view('product.vero.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add_vero()
    {
        return view('product.vero.addVero');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addvero(Request $request)
    {
        $validasi = Validator::make($request->all(), [
            'asin' => 'required|unique:tb_vero,asin',
            'brand' => 'required',
            'judul' => 'required',
            'keyword' => 'required',
        ], [
            'asin.required' => 'asin wajib diisi',
            'asin.unique' => 'Asin sudah ada',
            'brand.required' => 'brand wajib diisi',
            'judul.required' => 'judul wajib diisi',
            'keyword.required' => 'keyword wajib diisi',
        ]);

        if ($validasi->fails()) {
            return redirect('vero/add_vero')
                        ->withErrors($validasi)
                        ->withInput();
        } else {
            $datauser = [
                'asin' => $request->asin,
                'brand' => $request->brand,
                'judul' => $request->judul,
                'keyword' => $request->keyword,
                'penginput' => auth()->user()->name
                
            ];
          
            Vero::create($datauser);
            return redirect()->route('vero.add_vero')->with('success','User berhasil dibuat.');
        }
    }

    public function addasinvero(Request $request)
    {
        $validasi = Validator::make($request->all(), [
            'asin' => 'required|unique:tb_vero,asin',
            
        ], [
            'asin.required' => 'asin wajib diisi',
            'asin.unique' => 'Asin sudah ada',
        ]);

        if ($validasi->fails()) {
            return redirect('/vero/add_vero')
                        ->withErrors($validasi)
                        ->withInput();
        } else {
            $datauser = [
                'asin' => $request->asin,
                'brand' => "Vero Asin",
                'judul' => "Vero Asin",
                'keyword' => "Vero Asin",
                'penginput' => auth()->user()->name
                
            ];
          
            Vero::create($datauser);
            return redirect()->route('vero.add_vero')->with('success','Vero berhasil dimasukan.');
        }
    }

    public function addbrandvero(Request $request)
    {
        $validasi = Validator::make($request->all(), [
            'brand' => 'required|unique:tb_vero,brand',
            
        ], [
            'brand.required' => 'brand wajib diisi',
            'brand.unique' => 'brand sudah ada',
        ]);

        if ($validasi->fails()) {
            return redirect('/vero/add_vero')
                        ->withErrors($validasi)
                        ->withInput();
        } else {
            $datauser = [
                'asin' => "Vero Brand",
                'brand' => $request->brand,
                'judul' => "Vero Brand",
                'keyword' => "Vero Brand",
                'penginput' => auth()->user()->name
                
            ];
          
            Vero::create($datauser);
            return redirect()->route('vero.add_vero')->with('success','Vero berhasil dimasukan.');
        }
    }

    public function addjudulvero(Request $request)
    {
        $validasi = Validator::make($request->all(), [
            'judul' => 'required|unique:tb_vero,judul',
            
        ], [
            'judul.required' => 'judul wajib diisi',
            'judul.unique' => 'judul sudah ada',
        ]);

        if ($validasi->fails()) {
            return redirect('/vero/add_vero')
                        ->withErrors($validasi)
                        ->withInput();
        } else {
            $datauser = [
                'asin' => "Vero Judul",
                'brand' => "Vero Judul",
                'judul' => $request->judul,
                'keyword' => "Vero Judul",
                'penginput' => auth()->user()->name
                
            ];
          
            Vero::create($datauser);
            return redirect()->route('vero.add_vero')->with('success','Vero berhasil dimasukan.');
        }
    }
    public function addKeywordvero(Request $request)
    {
        $validasi = Validator::make($request->all(), [
            'keyword' => 'required|unique:tb_vero,keyword',
            
        ], [
            'keyword.required' => 'keyword wajib diisi',
            'keyword.unique' => 'keyword sudah ada',
        ]);

        if ($validasi->fails()) {
            return redirect('/vero/add_vero')
                        ->withErrors($validasi)
                        ->withInput();
        } else {
            $datauser = [
                'asin' => "Vero Keyword",
                'brand' => "Vero Keyword",
                'judul' => "Vero Keyword",
                'keyword' => $request->keyword,
                'penginput' => auth()->user()->name
                
            ];
          
            Vero::create($datauser);
            return redirect()->route('vero.add_vero')->with('success','Vero berhasil dimasukan.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy(Request $request)
    {
        $id = $request->post('id');
        $data = Vero::findOrFail($id);

        if($data->delete()){
            $response['success'] = 1;
            $response['msg'] = 'Delete successfully'; 
        }else{
            $response['success'] = 0;
            $response['msg'] = 'Invalid ID.';
        }
        return response()->json($response); 
     }


    public function import(Request $request)
{
    $file = $request->file('file');
    Excel::import(new VeroImportClass, $file);
    return redirect()->back()->with('success', 'Import successful');
}
}
