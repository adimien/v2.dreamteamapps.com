<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data_user = Account::all();
            return Datatables::of($data_user)
            ->addIndexColumn()
            ->addColumn('aksi', function ($data) {
                if (auth()->user()->role == 'superadmin')
                {
                    return "<button type='button' data-id='$data->id' data-jenis='edit' class='edit mb-2 btn btn-warning btn-icon-split mr-4'>
                    <span class='icon text-white-5'>
                        <i class='fas fa-edit'></i>
                    </span>
                    <span class='text'>Edit</span>
                  </button>";
                }else{
                    return '<div class="btn btn-danger">Not Access</div>';
                }
               
            })
            ->rawColumns(['aksi'])
            ->make(true);
        }
        return view('account.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $lister = User::get('name');
        return view('account.add_account', ['lister' => $lister]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validasi = Validator::make($request->all(), [
            'account_id' => 'required|unique:accounts,account_id',
            'lister' => 'required',
            'email' => 'required|email:dns',
        ], [
            'account_id.required' => 'User ID eBay wajib diisi',
            'account_id.unique' => 'User ID eBay sudah ada',
            'lister.required' => 'lister wajib diisi',
            'email.required' => 'email wajib diisi',
            'email.email' => 'email harus valid',
        ]);

        if ($validasi->fails()) {
            return redirect('/accounts/add_accs')
                        ->withErrors($validasi)
                        ->withInput();
        } else {
            $datauser = [
                'account_id' => $request->account_id,
                'email' => $request->email,
                'lister' => $request->lister
                
            ];
          
            Account::create($datauser);
            return redirect()->route('accounts.add_accs')->with('success','User ID berhasil dibuat.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Account::find($id);
        $alldata = User::all();
        return view ('account.edit',['data'=>$data,'alldata'=>$alldata]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $account_update = Account::find($id);
        $dataaccount = [
            'account_id' => $request->account_id,
            'email' => $request->email,
            'lister' => $request->lister
        ];
      
        $account_update->update($dataaccount);
        return redirect()->route('accounts')->with('success','Account berhasil diupdate.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
