<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\DataTables;

class UserController extends Controller
{
    public function register()
    {
        $data['title'] = 'Register';
        return view('user/register', $data);
    }

    // public function register_action(Request $request)
    // {
    //     $request->validate([
    //         'name' => 'required',
    //         'username' => 'required|unique:tb_user',
    //         'password' => 'required',
    //         'password_confirm' => 'required|same:password',
    //     ]);

    //     $user = new User([
    //         'name' => $request->name,
    //         'username' => $request->username,
    //         'password' => Hash::make($request->password),
    //     ]);
    //     $user->save();

    //     return redirect()->route('login')->with('success', 'Registration success. Please login!');
    // }


    public function login()
    {
        $data['title'] = 'Login';
        return view('user.login', $data);
    }
    public function aksi(Request $request)
    {
      
        $request->validate([
            'email' => 'required',
            'password' =>'required',
        ],[
            'email.required' => 'Email wajib diisi',
            'password.required' => 'password wajib diisi'
        ]);
        $infologin = [
            'email' => $request->email,
            'password' => $request->password
        ];
        if(Auth::attempt($infologin)){
            $request->session()->regenerate();
            return redirect('admin/home')->with('success', 'Berhasil login');
        }else{
            return redirect()->back()->withInput()->withErrors(['email' => 'Incorrect email or password.']);
        }
        
    }
    // public function login_action(Request $request)
    // {
    //     $request->validate([
    //         'email' => 'required',
    //         'password' => 'required',
    //     ]);
    //     if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
    //         $request->session()->regenerate();
    //         return redirect()->intended('/home');
    //     }

    //     return back()->withErrors([
    //         'password' => 'Wrong username or password',
    //     ]);
    // }

    public function password()
    {
        $data['title'] = 'Change Password';
        return view('user/password', $data);
    }

    public function password_action(Request $request)
    {
        $request->validate([
            'old_password' => 'required|current_password',
            'new_password' => 'required|confirmed',
        ]);
        $user = User::find(Auth::id());
        $user->password = Hash::make($request->new_password);
        $user->save();
        $request->session()->regenerate();
        return back()->with('success', 'Password changed!');
    }

    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect('/login');
    }
    public function list_user(Request $request)
    {
        if ($request->ajax()) {
            $data_user = User::all();
            return Datatables::of($data_user)
            ->addIndexColumn()
            ->addColumn('aksi', function ($data) {
                if (auth()->user()->role == 'superadmin')
                {
                    return '<div class="btn btn-primary">Aksi</div>';
                }else{
                    return '<div class="btn btn-danger">Not Access</div>';
                }
               
            })
            ->rawColumns(['checkbox','aksi','title','price','penginput_barang','listing','action'])
            ->make(true);
        }
        return view ('user.list');
    }

    public function add_user()
    {
        return view('user.add');
    }
    public function adduser(Request $request)
    {
        $validasi = Validator::make($request->all(), [
            'name' => 'required|unique:users,name',
            'email' => 'required',
            'password' => 'required',
            'role' => 'required',
        ], [
            'name.required' => 'Name wajib diisi',
            'name.unique' => 'n\Name sudah ada',
            'email.required' => 'email wajib diisi',
            'password.required' => 'password wajib diisi',
            'role.required' => 'role wajib diisi',
        ]);

        if ($validasi->fails()) {
            return redirect('user/register')
                        ->withErrors($validasi)
                        ->withInput();
        } else {
            $datauser = [
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'role' => $request->role
                
            ];
          
            User::create($datauser);
            return redirect()->route('user.add')->with('success','User berhasil dibuat.');
        }
    }
}