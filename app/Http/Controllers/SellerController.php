<?php

namespace App\Http\Controllers;

use App\Models\Seller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;
use Carbon\Carbon;

class SellerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data_seller = Seller::all();
        return view('seller.index',['data_seller'=>$data_seller]);
    }
    public function list_seller(Request $request)
    {

        if ($request->ajax()) {
            $last14Days = Carbon::now()->subDays(-14);
            $data_seller = Seller::where('updated_at', '>=', $last14Days)->get();

            if($data_seller->isEmpty()){
                $data_seller = Seller::where('penginput_seller',auth()->user()->name)->get();
            }
            return Datatables::of($data_seller)
            ->addIndexColumn()
            ->addColumn('seller', function ($data_seller){
                if($data_seller->status_seller =='Pakai'){
                    return '<span class="badge badge-danger">Not Available</span>';
                }else{
                    return '<span class="badge badge-success">' . $data_seller->seller_ebay . '</span>';

                }
            })
            ->addColumn('status', function ($data_seller) {
                $last14Days = Carbon::now()->subDays(14);
                $updatedAt = Carbon::parse($data_seller->updated_at);
                if($updatedAt = $last14Days){
                    return '<span class="badge badge-primary">Sudah di Gunakan</span>';
                }
                else{  
                    return '<span class="badge badge-danger">Not Available</span>';
                }
            })     
            ->addColumn('aksi', function ($data_seller) {
                $last14Days = Carbon::now()->subDays(14);
                $updatedAt = Carbon::parse($data_seller->updated_at);
                if($updatedAt->greaterThanOrEqualTo($last14Days)) {
                    return '<button type="button" data-id="' . $data_seller->id . '" data-jenis="edit" class="btn btn-warning btn-icon-split action">
                            <span class="icon text-white-5">
                                <i class="fas fa-edit"></i>
                            </span>
                            <span class="text">Pakai</span>
                          </button>';
                }
                return '<button type="button" disabled data-id="' . $data_seller->id . '" data-jenis="edit" class="btn btn-warning btn-icon-split action">
                            <span class="icon text-white-5">
                                <i class="fas fa-edit"></i>
                            </span>
                            <span class="text">Pakai</span>
                          </button>';
            })
            
                // return view('seller.tombol')->with('data_seller', $data_seller);
            
            ->addColumn('last_used', function ($data_seller) {
                $date_last = Carbon::parse($data_seller->updated_at);
               return $date_last->format('d-M-Y h:m A');               
            })
            
            ->addColumn('bisa_pakai', function ($data_seller) {
                $expiryDate = Carbon::parse($data_seller->updated_at)->addDays(14);
    
                // Format the date to the desired format
                return $expiryDate->format('d-M-Y h:m A');               
             })
             
            ->rawColumns(['seller','last_used','bisa_pakai','status','aksi'])
            ->make(true);
        }
        return view('seller.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add_seller()
    {
        return view ('seller.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validasi = Validator::make($request->all(), [
            'seller_ebay' => 'required|unique:seller,seller_ebay'
        ], [
            'seller_ebay.required' => 'Seller ID wajib diisi',
            'seller_ebay.unique' => 'Seller ID sudah ada',
        ]);

        if ($validasi->fails()) {
            return redirect('seller/addseller')
                        ->withErrors($validasi)
                        ->withInput();
        } else {
            $dataseller = [
                'seller_ebay' => $request->seller_ebay,
                'penginput_seller' => $request->penginput_seller,
                
            ];
          
            Seller::create($dataseller);
            return redirect()->route('seller.add_seller')->with('success','Seller ID berhasil dibuat.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
        {
            $data_seller = Seller::find($id);
            return view('seller.edit', compact('data_seller'));
        }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
        {
            $seller_update = Seller::find($id);
            $dataseller = [
                'status_seller' => $request->status_seller,
                'ket_seller' => $request->ket_seller,
                'penginput_seller' => $request->penginput_seller
            ];
          
            $seller_update->update($dataseller);
            return response()->json([
                'redirect' => route('seller')
              ]);
        }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
