<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\product;
use App\Models\User;
use Illuminate\Http\Request;
use Carbon\Carbon;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
        
        $lister = User::all();
        $username = User::select('name')->get();
        $product = product::all();
        $account = Account::all();
        $count_input = Product::whereDate('created_at', today())->count();
        $count_item = Product::whereNull('account_ebay')->count();
        $count_listed = Product::whereDate('listed_at', '=', today())->count();
        $input_today ="";
        return view ('product.report.index', ['lister' => $lister, 'product' => $product,'account_list'=>$account,'count_ready' =>  $count_item,'count_listed'=>$count_listed,'count_input'=>$count_input,'penginput_today'=>$input_today,'username'=>$username]);
    }
   public function countBarang()
    {
        $penginput = request('penginput');
        $startDate = request('startDate');
        $endDate = request('endDate');
        $count_input = Product::where('created_at', '=', today())->count();
        $count_item = Product::whereNull('account_ebay')->count();
        $count_listed = Product::where('listed_at', '=', today())->count();
        

        // Validate input
        if (empty($penginput) || empty($startDate) || empty($endDate)) {
            return redirect()->back()->with('error', 'Please fill in all required fields.');
        }

        $count = Product::where('penginput', '=', $penginput)
        ->whereBetween('created_at', [$startDate, $endDate])
        ->count();
        $lister = User::all();
        $product = product::all();
        $account = Account::all();
        
        return view('product.report.index', [
            'lister' => $lister,
            'penginput' => $penginput,
            'product' => $product,
            'count' => $count,
            'account_list'=>$account,
            'startDate' => $startDate,
            'endDate' => $endDate,'count_ready' =>  $count_item,'count_listed'=>$count_listed,'count_input'=>$count_input
        ]);
    }
    public function countlisting()
    {
        $account_ebay = request('account_ebay');
        $startDate = request('startDate');
        $endDate = request('endDate');
        $count_input = Product::where('created_at', '=', today())->count();
        $count_item = Product::whereNull('account_ebay')->count();
        $count_listed = Product::where('listed_at', '=', today())->count();

        // Validate input
        if (empty($account_ebay) || empty($startDate) || empty($endDate)) {
            return redirect()->back()->with('error', 'Please fill in all required fields.');
        }

        $count_listing = Product::where('account_ebay', '=', $account_ebay)
        ->whereBetween('listed_at', [$startDate, $endDate])
        ->count();
        $lister = User::all();
        $product = product::all();
        $account = Account::all();
        
        return view('product.report.index', [
            'lister' => $lister,
            'account_list' => $account,
            'account_ebay' => $account_ebay,
            'product' => $product,
            'count_listing' => $count_listing,
            'startDate' => $startDate,
            'endDate' => $endDate,'count_ready' =>  $count_item,'count_listed'=>$count_listed,'count_input'=>$count_input
        ]);
    }
    
    
    public function countbrangReady()
    {
        $account_ebay = request('account_ebay');
        $startDate = request('startDate');
        $endDate = request('endDate');
        

        // Validate input
        if (empty($account_ebay) || empty($startDate) || empty($endDate)) {
            return redirect()->back()->with('error', 'Please fill in all required fields.');
        }

       
        $lister = User::all();
        $product = product::all();
        $account = Account::all();
        
        return view('product.report.index', [
            'lister' => $lister,
            'account_list' => $account,
            'account_ebay' => $account_ebay,
            'product' => $product,
            'count_item' => $count_item,
            'startDate' => $startDate,
            'endDate' => $endDate,'count_ready' =>  $count_item,'count_listed'=>$count_listed,'count_input'=>$count_input
        ]);
    }

    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
