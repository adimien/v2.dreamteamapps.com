<?php

namespace App\Http\Controllers;

use App\Models\Account;
use DateTime;
use App\Models\product;
use App\Models\Vero;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class productController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $account = Account::where('lister', auth()->user()->name)->get();
        return view('product.index',['account'=>$account]);
    }
    public function list_item(Request $request)
    {
        
        if ($request->ajax()) {
            if ($request->account_ebay != null) {
                $data = Product::where('account_ebay', $request->account_ebay)->get();
            } else if (auth()->user()->role == 'lister') {
                $data = Product::where(function($query) {
                    $query->whereHas('user', function ($subquery) {
                        $subquery->where('role', '=', 'admin');
                    })->orWhere('penginput', '=', auth()->user()->name);
                })->orderby('account_ebay','ASC')->get();
            } else if (auth()->user()->role == 'superadmin') {
                $data = Product::orderBy('account_ebay', 'asc')->latest()->get();
            }
           
         return Datatables::of($data)
                ->addColumn('checkbox', function ($data) {
                    if ($data->account_ebay !== null) {
                        return '';
                    } 
                    return '<input type="checkbox" name="product_checkbox[]" class="product_checkbox" value="'.$data->id.'" />';
                })
                ->addIndexColumn()
                ->addColumn('action', function ($data) {
                    return '<a href="https://amazon.com/dp/'.$data->asin.'" target="_blank">'.$data->asin.'</a><br>';
                })
                ->addColumn('title', function ($data) {
                    return '<div class="pd-5" style="font-size: 15px;"><b>Judul Amazon : </b>'.$data->judul.'</div><br><b>Judul eBay :</b><br>'.Str::limit($data->judul_eb, 80);
                })
                ->addColumn('price', function ($data) {
                    return '<div class="pd-5" style="font-size: 15px;"> <img src="'.asset('img/315-3159325_amazon.png').'" class="mb-3" width=35px><br>$'.$data->harga.'</div><br><img src="'.asset('img/143967.png').'" class="mb-3" width=45px> <br>$'.$data->harga_eb;
                })
                ->addColumn('penginput_barang', function($data){
                    return '<div style="text-align: center;">' . $data->penginput . '</div><br><div style="text-align: center;">' . \Carbon\Carbon::parse($data->created_at)->format('d-M-Y') . '<br><div style="text-align: center;">' . \Carbon\Carbon::parse($data->created_at)->format('h:i A').'</div>';
                })
                ->addColumn('penginput_barang', function($data){
                    return '<div style="text-align: center;">' . $data->penginput . '</div><br><div style="text-align: center;">' . \Carbon\Carbon::parse($data->created_at)->format('d-M-Y') . '<br><div style="text-align: center;">' . \Carbon\Carbon::parse($data->created_at)->format('h:i A').'</div>';
                })
                ->addColumn('listing', function ($data) {
                    return $data->account_ebay>0? view('product.column')->with('data', $data):"";
                })
                
                ->addColumn('aksi', function ($data) {
                    $image = asset($data->image);
                    return '<div style="display: flex; justify-content: center; align-items: center; height: 100%;"><img src="'.$image.'" alt="Image" width="100px"></div><p style="text-align: center;"><button type="button" class="btn btn-outline-primary mt-3">Brand: '.$data->brand.'</button></p>';
                })
                ->rawColumns(['checkbox','aksi','title','price','penginput_barang','listing','action'])
                ->make(true);
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search_account(Request $request)
    {
        $output ="";
        $account = $request->search_acc;
        $listingBanyak = product::whereIn('account_ebay', $account)->get();
        
        return response($output);
    }
    public function create()
    {  
       
       return view('product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validasi = Validator::make($request->all(), [
            'asin' => 'required|unique:tb_product,asin|unique:tb_vero,asin',
            'judul' => 'required',
            'harga' => 'required',
            'image_url' => 'required',
            'brand' => 'required|unique:tb_vero,brand',
        ], [
            'asin.required' => 'Asin wajib diisi',
            'asin.unique' => 'Asin Sudah ada, silakan ganti Asin anda',
            'judul.required' => 'Judul wajib diisi',
            'harga.required' => 'Harga wajib diisi',
            'brand.required' => 'Brand wajib diisi',
            'image_url.required' => 'Link Gambar wajib diisi',
            'brand.unique' =>'Tidak boleh memasukan Brand ini',
        ]);

        if ($validasi->fails()) {
            return redirect('ProductList/create')
                        ->withErrors($validasi)
                        ->withInput();
        } else {
           
            // Retrieve an array of words from another table
            $words_to_remove = Vero::get('keyword')->pluck('keyword')->toArray();
            $title = $request->judul_eb;
            $title_correction =str::replace($words_to_remove,[''], $title);

            // Create the data array to insert into the table
            $data = [
                'asin' => $request->asin,
                'judul' => $request->judul,
                'harga' => $request->harga,
                'brand' => $request->brand,
                'image' => $request->image_url,
                'judul_eb' => $title_correction,
                'harga_eb' => $request->harga_eb,
                'penginput' => $request->penginput,
                'penginput_id' => auth()->user()->id
            ];

            // Insert data into the table
            product::create($data);

            return redirect()->route('ProductList.create')->with('success','Produk berhasil disimpan.');
        }
    }
    public function getProduct()
    {
        
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
	{
		$barang = product::find($id)->first();

		return view('product.edit', ['barang' => $barang]);
	}
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = product::findOrFail($id);
        $data->delete();
    }
    function removeall(Request $request)
    {
        $user_id_array = $request->input('id');
        $user = product::whereIn('id', $user_id_array);
        if($user->delete())
        {
            echo 'Data Deleted';
        }
    }
   function listing(Request $request)
    {
        
        $item_array = $request->input('id');
        $item_list = $request->input('account_ebay');
        $lister = $request->input('lister');
        $listingBanyak = product::whereIn('id', $item_array);
        foreach ($item_array as $key){
            $data = array(
                'account_ebay' => $item_list,
                'lister' => $lister,
                'listed_at' => Carbon::now()
            );
            $listingBanyak->update($data, $key);
            return response()->json(['success' => "Barang Berhasil di listing"]);
        }
        

    }
    public function checkAsin(Request $request)
    {
        $asin = $request->input('asin');

        if(!empty($asin)) {
            $cek_asin = product::where('asin', $asin)->count();
            $cek_asinvero = Vero::where('asin', $asin)->count();
            if($cek_asin > 0){
                return '<div class="text-red alert alert-danger" style="padding: 7px;padding-left: 17px;">Asin sudah ada, silahkan ganti dengan yang lain</div>';
            }elseif($cek_asinvero >0){
                return '<div class="text-red alert alert-danger" style="padding: 7px;padding-left: 17px;"><i class="fas fa-fw fa-exclamation-triangle"></i> Asin masuk list vero, silahkan ganti dengan yang lain</div>';
            }else {
                return '<div class="text-green alert alert-success" style="padding: 7px;padding-left: 17px;">ASIN Tersedia dan aman</div>';
            } 
        } else {
            return "";
        }
    }
    public function checkBrand(Request $request)
    {
        $brand = $request->input('brand');

        if(!empty($brand)) {
            $cek_brandVero = Vero::where('brand', $brand)->count();
            if($cek_brandVero >0){
                return '<div class="text-red alert alert-danger" style="padding: 7px;padding-left: 17px;"><i class="fas fa-fw fa-exclamation-triangle"></i> Brand/Merek masuk list vero, silahkan ganti dengan yang lain</div>';
            }else {
                return '<div class="text-green alert alert-success" style="padding: 7px;padding-left: 17px;">BRAND/Merek Tersedia dan aman</div>';
            } 
        } else {
            return "";
        }
    }

}
