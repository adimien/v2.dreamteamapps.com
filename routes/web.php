<?php

use App\Http\Controllers\AccountController;
use App\Http\Controllers\productController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StudentController;

use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProductAdminController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\SellerController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\VeroController;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//0 = superadmin 1=admin 2=lister 3=riset
// Route::get("/admin/product",[productController::class, 'index'])->name("admin.product");
Route::get('/', [UserController::class, 'login']);
Route::get('/admin', [UserController::class, 'login'])->name('admin.login');
Route::post('/user/login', [UserController::class, 'aksi']);

Route::middleware(['guest'])->group(function()
{
});
Auth::routes();
Route::middleware(['auth'])->group(function()
{   
    Route::get("admin/home",[HomeController::class, 'Home'])->name("home");
    Route::get('user', [UserController::class, 'list_user'])->name('user');
    Route::get('user/register', [UserController::class, 'add_user'])->name('user.add');
    Route::post('user/adduser', [UserController::class, 'adduser'])->name('user.adduser');
    Route::post('/check_asin', [productController::class, 'checkAsin'])->name('check_asin');
    Route::post('/check_brand', [productController::class, 'checkBrand'])->name('check_brand');
    
    Route::resource('ProductList', productController::class);
    Route::resource('filter_akun', productController::class);
    Route::controller(productController::class)->prefix('product')->group(function () {
    Route::get('edit/{id}', 'edit')->name('barang.edit');
    Route::post('listing', 'listing')->name('barang.listing'); 
       
    });
    Route::get('admin/product',[productController::class,'index'])->name("product");
    Route::get('product/filter_akun', [productController::class, 'search_account'])->name('product.search_account');    
    Route::get('product/removeall', [productController::class, 'removeall'])->name('product.removeall');   
    Route::get('product/list_item',[productController::class, 'list_item']); 
    Route::get('/product/create', function () { return view('product.create');});

    Route::get('/vero',[VeroController::class, 'index'])->name("vero");
    Route::get('/vero/add_vero',[VeroController::class, 'add_vero'])->name('vero.add_vero');
    Route::post('/vero/addvero',[VeroController::class, 'addvero'])->name('vero.addvero');
    Route::post('/vero/addasinvero',[VeroController::class, 'addasinvero'])->name('vero.addasinvero');
    Route::post('/vero/addbrandvero',[VeroController::class, 'addbrandvero'])->name('vero.addbrandvero');
    Route::post('/vero/addjudulvero',[VeroController::class, 'addjudulvero'])->name('vero.addjudulvero');
    Route::post('/vero/addKeywordvero',[VeroController::class, 'addKeywordvero'])->name('vero.addKeywordvero');
    Route::post('/vero/import',[VeroController::class, 'import'])->name('vero.import');
    Route::get('/vero/edit/{id}',[VeroController::class, 'edit'])->name('vero.edit');
    Route::post('/vero/update/{id}',[VeroController::class, 'update'])->name('vero.update');
    Route::get('/vero/delete/{id}',[VeroController::class, 'delete'])->name('vero.delete');
    Route::post('/vero/destroy',[VeroController::class, 'destroy'])->name('vero.destroy');

    Route::get('/seller', [SellerController::class, 'index'])->name('seller');
    Route::get('/seller/add_seller',[SellerController::class, 'add_seller'])->name('seller.add_seller');
    Route::post('/seller/addseller',[SellerController::class, 'addseller'])->name('seller.addseller');
    Route::get('/seller/edit/{id}', [SellerController::class, 'edit'])->name('seller.edit');
    Route::post('/seller/update/{id}', [SellerController::class, 'update'])->name('seller.update');
    Route::get('/seller/list_seller',[SellerController::class,'list_seller'])->name('seller.list_seller');
    Route::resource('/seller/addseller', SellerController::class);

    
    Route::get('/account',[AccountController::class, 'index'])->name("accounts");
    Route::get('/accounts/add_accs',[AccountController::class, 'create'])->name('accounts.add_accs');
    Route::get('/accounts/edit/{id}',[AccountController::class, 'edit'])->name('accounts.edit');
    Route::post('/accounts/addacc',[AccountController::class, 'store'])->name('accounts.addacc');
    Route::post('/accounts/update/{id}', [AccountController::class, 'update'])->name('accounts.update');

    Route::get('/report', [ReportController::class, 'index'])->name('report');
    Route::get('/report/count', [ReportController::class, 'countBarang'])->name('report.countBarang');
    Route::get('/report/countlisting', [ReportController::class, 'countlisting'])->name('report.countlisting');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
