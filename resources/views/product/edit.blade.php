@extends('layout.template')
@section('isi')
@extends('layout.template')
<!-- End of Topbar -->
@section('isi')
               
               <!-- Begin Page Content -->
               <div class="container-fluid">

                <!-- Page Heading -->
                <h1 class="h3 mb-2 text-gray-800">Tables</h1>
                <p class="mb-4">DataTables is a third party plugin that is used to generate the demo table below.
                    For more information about DataTables, please visit the <a target="_blank"
                        href="https://datatables.net">official DataTables documentation</a>.</p>                  
                <!-- DataTales Example -->


                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary"> <a href='{{url('product')}}' class="btn btn-warning">+ Kembali</a></h6>
                    </div>
                    
                    <div class="card-body">
                        <form action="{{ route('ProductList.store') }}" method="POST">
                            @csrf
                        @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                        @endif
                        <div class="mb-3 row">
                            <label for="nama" class="col-sm-2 col-form-label">ASIN :</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" name='asin' id="asin"  value="{{old('asin')}}">
                                @error('asin')
                                <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="email" class="col-sm-2 col-form-label">Judul :</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name='judul' id="judul">
                                @error('judul')
                                <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="email" class="col-sm-2 col-form-label">Harga :</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" name='harga' id="harga">
                                @error('harga')
                                <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary ml-3">Submit</button>
                        </form>
                    </div>
                </div>
            </div>

                <!-- /.container-fluid -->

@endsection