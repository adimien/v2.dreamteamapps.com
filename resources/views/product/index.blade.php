@extends('layout.template')
<!-- End of Topbar -->
@section('isi')
               
                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-2 text-gray-800">Data Barang</h1>
                    <p class="mb-4">Pengumuman :<br> Dilarang mencuri barang lister lain.</p>

                    <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                            @endif
                            <div class="alert alert-danger d-none"></div>
                           <div class="alert alert-success d-none"></div>
                         @if(auth()->user()->role == 'superadmin')
                            <a href='{{url('ProductList/create')}}' class="btn btn-primary">+ Tambah Barang</h6></a>
                            <button type="button" disabled id="btn-list-all" data-toggle="modal" data-target="#modal-list"class="btn btn-success">+ Listing Barang</button>
                            <button type="button" disabled name="bulk_delete" id="bulk_delete"class="btn btn-danger">- Delete Barang</button>
                            <div class="modal fade" id="modal-list" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h5 class="modal-title" id="exampleModalLabel">Bulk Listing item</h5>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                        <label for="">Pilih Akun Tujuan :</label>
                                        <select class="form-control"id="listing-banyak">
                                            @foreach($account as $acc)
                                            <option value="{{ $acc->account_id}}">{{ $acc->account_id}}</option>
                                             @endforeach
                                        </select>
                                      <input type="text" id="lister" class="form-control mt-4" value="{{auth()->user()->name}}" hidden>
                                   
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                      <button class="btn btn-sm btn-success" id="btn_update" type="button">Listing</button>
                                    </div>
                                </div>
                            </div>
                              </div>
                                <div class="form-group"  style="margin-top: 20px;">
                                    <label><strong>Filter Akun:</strong></label>
                                    <select id='account_ebay' class="form-control filter" style="width: 200px" >
                                        <option value="">--Akun eBay</option>
                                        @foreach($account as $acc)
                                        <option value="{{ $acc->account_id}}">{{ $acc->account_id}}</option>
                                      @endforeach
                                    </select>
                                </div>
                            @elseif(auth()->user()->role == 'lister')
                            <a href='{{url('ProductList/create')}}' class="btn btn-primary">+ Tambah Barang</h6></a>
                            <button type="button" disabled id="btn-list-all" data-toggle="modal" data-target="#modal-list"class="btn btn-success">+ Listing Barang</button>
                            <button type="button" disabled name="bulk_delete" id="bulk_delete"class="btn btn-danger">- Delete Barang</button>
                            <div class="modal fade" id="modal-list" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h5 class="modal-title" id="exampleModalLabel">Bulk Listing item</h5>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                        <label for="">Pilih Akun Tujuan :</label>
                                        <select class="form-control"id="listing-banyak">
                                            @foreach($account as $acc)
                                            <option value="{{ $acc->account_id}}">{{ $acc->account_id}}</option>
                                              @endforeach
                                        </select>
                                        <input type="text" id="lister" class="form-control mt-4" value="{{auth()->user()->name}}" hidden>
                                      
                                   
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                      <button class="btn btn-sm btn-success" id="btn_update" type="button">Listing</button>
                                    </div>
                                </div>
                            </div>
                              </div>
                                <div class="form-group"  style="margin-top: 20px;">
                                    <label><strong>Filter Akun:</strong></label>
                                    <select id='account_ebay' class="form-control filter" style="width: 200px" >
                                        <option value="">--Akun eBay</option>
                                        @foreach($account as $acc)
                                        <option value="{{ $acc->account_id}}">{{ $acc->account_id}}</option>
                                         @endforeach
                                    </select>
                                </div>
                            @else
                            <a href='{{url('ProductList/create')}}' class="btn btn-primary">+ Tambah Barang</h6></a>
                           <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label><strong>Filter Akun:</strong></label>
                                    <select id='account_ebay' class="form-control filter">
                                        <option value="">--Akun eBay</option>
                                        @foreach($account as $acc)
                                            <option value="{{ $acc->account_id}}">{{ $acc->account_id}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label><strong>Filter Lister:</strong></label>
                                    <select id='lister' class="form-control filter" >
                                        <option value="">--Lister</option>
                                        <option value="admin">admin</option>
                                        <option value="riset">riset</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label><strong>Tanggal Input:</strong></label>
                                    <select id='account_ebay' class="form-control filter">
                                        <option value="">--Lister</option>
                                        <option value="admin">admin</option>
                                        <option value="riset">riset</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label><strong>Tanggal Input:</strong></label>
                                    <select id='account_ebay' class="form-control filter">
                                        <option value="">--Lister</option>
                                        <option value="admin">admin</option>
                                        <option value="riset">riset</option>
                                    </select>
                                </div>
                            </div>
                           </div>
                            @endif
                        </div>

                        <div class="card-body">
                        <form  id="fm-listing">
                            <div class="table-responsive">
                                <table class="table table-bordered all-data" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th><input type="checkbox" id="hd-cb"></th>
                                            <th>No</th>
                                            <th>Asin</th>
                                            <th class="col-md-4">Judul</th>
                                            <th>Harga</th>
                                            <th>Penginput</th>
                                            <th style="text-align: center;">Account eBay</th>
                                            <th>IMAGE</th>
                                        </tr>
                                    </thead>
                                </table>
                                
                            </div>
                        </form>
                        </div>
                    </div>
                </div>

                <!-- Modal -->


              
                <div class="modal fade" id="confirmModal" tabindex="-1" aria-labelledby="ModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                    <div class="modal-content">
                    <form method="post" id="sample_form" class="form-horizontal">
                        <div class="modal-header">
                            <h5 class="modal-title" id="ModalLabel">Confirmation</h5>
                            <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <h4 align="center" style="margin:0;">Are you sure you want to remove this data?</h4>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="button" name="ok_button" id="ok_button" class="btn btn-danger">OK</button>
                        </div>
                    </form>  
                    </div>
                    </div>
                </div>


{{-- <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Tambah Barang</h5>
            <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <!-- START FORM -->
            <div class="alert alert-danger d-none"></div>
            <div class="alert alert-success d-none"></div>
            @csrf
            <div class="mb-3 row">
                <label for="nama" class="col-sm-2 col-form-label">ASIN</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name='asin' id="asin">
                </div>
            </div>
            <div class="mb-3 row">
                <label for="email" class="col-sm-2 col-form-label">Judul</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name='judul' id="judul">
                </div>
            </div>
            <div class="mb-3 row">
                <label for="email" class="col-sm-2 col-form-label">Harga</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name='harga' id="harga">
                </div>
            </div>
            <!-- AKHIR FORM -->
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary tombol-simpan">Simpan</button>
        </div>
      </div>
    </div>
  </div> --}}
                <!-- /.container-fluid -->
                @include('product.script')
@endsection