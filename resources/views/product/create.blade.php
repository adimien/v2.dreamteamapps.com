@extends('layout.template')
<!-- End of Topbar -->
@section('isi')
               
               <!-- Begin Page Content -->
               <div class="container-fluid">

                <!-- Page Heading -->
                <h1 class="h3 mb-2 text-gray-800">Input Data Barang</h1>
                <p class="mb-4">Data barang hanya diperbolehkan dengan harga amazon minimal $4.99, dan selisih dengan kompetitor maksimal $5.99.</p>                  
                <!-- DataTales Example -->


                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary"> <a href='{{url('admin/product')}}' class="btn btn-warning">+ Kembali</a></h6>
                    </div>
                    
                    <div class="card-body">
                        <form action="{{ route('ProductList.store') }}" method="POST">
                            @csrf
                        @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                        @endif
                      
                        <div class="mb-3 row">
                            <label for="asin" class="col-sm-2 col-form-label">ASIN</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" name='asin' id="asin"  onChange="checkProductCode()" value="{{old('asin')}}">
                                @error('asin')
                                <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="col-sm-4">
                                <span id="asin-status" ></span>
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="brand" class="col-sm-2 col-form-label">BRAND / Merek</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" name='brand' id="brand"  onChange="checkBrand()" value="{{old('brand')}}">
                                @error('brand')
                                <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="col-sm-4">
                                <span id="brand-status" ></span>
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="brand" class="col-sm-2 col-form-label">Link Gambar Produk</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" name='image_url' id="image_url" >
                                @error('image_url')
                                <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="col-sm-4">
                                <span id="brand-status" ></span>
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="judul" class="col-sm-2 col-form-label">Judul Amazon</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name='judul' id="judul">
                                @error('judul')
                                <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="harga" class="col-sm-2 col-form-label">Harga</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" name='harga' id="harga" onkeyup="estBe()" onchange="estBe()">
                                @error('harga')
                                <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                                @enderror
                            </div>
                            <label for="harga" class="col-form-label">Est. Sell ($)</label>
                            <div class="col-sm-2">
                                <div class="form-control" id="estSell" readonly></div>
                            </div>
                            <label for="harga" class="col-form-label">BE</label>
                            <div class="col-sm-1">
                                <div class="form-control" id="BeSell" readonly></div>
                            </div>
                                 
                        </div>
                        <div class="mb-3 row">
                            <label for="judul_eb" class="col-sm-2 col-form-label">Judul eBay</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name='judul_eb' id="judul_eb">
                                @error('judul_eb')
                                <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="harga_eb" class="col-sm-2 col-form-label">Harga </label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" name='harga_eb' id="harga_eb" onkeyup="Becomp()" onchange="Becomp()">
                                @error('harga_eb')
                                <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                                @enderror
                            </div>
                            <label for="harga" class="col-form-label">BE Competitor (%)</label>
                            <div class="col-sm-1">
                                <div class="form-control" id="bEcomp" readonly></div>
                            </div>

                            <label for="harga" class="col-form-label">Selisih ($)</label>
                            <div class="col-sm-1">
                               <div class="form-control" id="selisih" readonly></div>
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="harga_eb" class="col-sm-2 col-form-label">Penginput</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" name='penginput' id="penginput" value="{{ Auth::user()->name }}" readonly>
                                <input type="text" class="form-control" name='penginput_id' id="penginput_id" value="{{ Auth::user()->id }}" hidden>
                                @error('penginput')
                                <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                                @enderror
                            </div>
                            
                        </div>
                        <button type="submit" id="btn-simpan" class="btn btn-primary ml-3">Submit</button>
                        </form>
                    </div>
                </div>
            </div>

                <!-- /.container-fluid -->
            <script>
              function checkProductCode(){
                var asin = $('#asin').val();
                $.ajax({
                  url:"{{ route('check_asin')}}",
                  headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                  method:"post",
                  data:{
                   
                    asin:asin
                },
                success:function(data)
                {
                    $("#asin-status").html(data);
                }
                   
                });
              };
              function checkBrand(){
                var brand = $('#brand').val();
                $.ajax({
                  url:"{{ route('check_brand')}}",
                  headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                  method:"post",
                  data:{
                   
                    brand:brand
                },
                success:function(data)
                {
                    $("#brand-status").html(data);
                }
                   
                });
              };
              function estBe() {
                var priceAz = $('#harga').val();
                var priceEb = $('#harga_eb').val();
                var set_profit = 0.5;
                var set_BeSell = 35;
                let est_sell = (parseFloat(priceAz) + parseFloat(set_profit)) * 100 / (100 - set_BeSell);
                document.getElementById("estSell").innerHTML = est_sell.toFixed(2);
                document.getElementById("BeSell").innerHTML = set_BeSell;
                var harga_sell = document.getElementById("BeSell").innerHTML;
                if(priceAz<=4.99){
                    $('#btn-simpan').prop('disabled', true);
                    } else {
                    $('#btn-simpan').prop('disabled', false);
                    }
                $.ajax({
                    url: "{{ route('ProductList.store') }}",
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type: "POST",
                    data: { harga_sell: harga_sell },
                    success: function(response) {
                        console.log(response)
                    }
                });
            }

             function Becomp(){
                var priceAz = $('#harga').val();
                var priceEb = $('#harga_eb').val();
                var set_profit = 0.5;
                var set_BeSell = 35;
                var est_sell = (parseFloat(priceAz) + parseFloat(set_profit)) * 100 / (100 - set_BeSell);
                let est_beComp = 100*(parseFloat(priceEb) - parseFloat(priceAz)-set_profit)/parseFloat(priceEb);
                document.getElementById("bEcomp").innerHTML = est_beComp.toFixed();
                var harga_sell = document.getElementById("BeSell").innerHTML;

                var selisih = parseFloat(est_sell) - parseFloat(priceEb);
                document.getElementById("selisih").innerHTML = selisih.toFixed(2);
                var selisih_harga = document.getElementById("selisih").innerHTML;
                if (selisih_harga >= 5.99) {
                $('#btn-simpan').prop('disabled', true);
                } else {
                $('#btn-simpan').prop('disabled', false);
                }

             }


            </script>
@endsection