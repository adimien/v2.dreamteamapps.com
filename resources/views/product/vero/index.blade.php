@extends('layout.template')
<!-- End of Topbar -->
@section('isi')
               
                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-2 text-gray-800">Data Vero</h1>
                   <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                            @endif
                            <div class="alert alert-danger d-none"></div>
                           <div class="alert alert-success d-none"></div>   
                           <a href='{{url('vero/add_vero')}}' class="btn btn-danger"> <i class="fas fa-fw fa-exclamation-triangle"></i> Tambah Data Vero</h6></a>                   
                        </div>

                        <div class="card-body">
                        <form  id="fm-listing">
                            <div class="table-responsive">
                                <table class="table table-bordered all-data" id="VeroTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Asin</th>
                                            <th>Brand</th>
                                            <th>Judul</th>
                                            <th >Kata /Keyword</th>
                                            <th>CREATED BY</th>
                                            <th class="col-sm-2">DATE INPUT</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                </table>
                                
                            </div>
                        </form>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="confirmModalw" tabindex="-1" aria-labelledby="ModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                    <div class="modal-content">
                    <form method="post" id="sample_form" class="form-horizontal">
                        <div class="modal-header">
                            <h5 class="modal-title" id="ModalLabel">Confirmation</h5>
                            <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <h4 align="center" style="margin:0;">Are you sure you want to remove this data?</h4>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="button" name="ok_button" id="ok_button" class="btn btn-danger">OK</button>
                        </div>
                    </form>  
                    </div>
                    </div>
                </div>

                @include('product.vero.script')
@endsection