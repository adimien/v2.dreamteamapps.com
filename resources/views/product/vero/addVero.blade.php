@extends('layout.template')
<!-- End of Topbar -->
@section('isi')
               
               <!-- Begin Page Content -->
               <div class="container-fluid">

                <!-- Page Heading -->
                <h1 class="h3 mb-2 text-gray-800">Tambah Data Vero</h1>
                
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary"> <a href='{{url('vero')}}' class="btn btn-warning">+ Kembali</a></h6>
                    </div>

                   
                    <div class="card-body">
                            <div class="mb-3 row">
                                <label for="brand" class="col-sm-2 col-form-label">Pilih jenis Vero</label>
                                <div class="col-sm-4">
                                    <select name="vero-type" id="vero-type" class="form-control">
                                        <option value="">Select Type Vero</option>
                                        <option value="asin" id="asin">ASIN</option>
                                        <option value="brand" id="brand">Brand</option>
                                        <option value="judul" id="title">Judul</option>
                                        <option value="keyword" id="keyword">Keyword</option>
                                    </select>
                                </div>
                            </div>
                            @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                            @endif
                              @error('asin')
                                    <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                                @enderror
                             @error('brand')
                                <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                             @enderror
                             @error('judul')
                                <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                            @enderror
                             @error('keyword')
                                        <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                            @enderror
                            <div class="mb-3 row" id="collapse-asin" style="display: none;">
                                <label for="asin" class="col-sm-2 col-form-label">ASIN</label>
                                <div class="col-sm-4">
                                    <form action="{{url('/vero/addasinvero')}}" method="post">
                                        @csrf
                                      
                                        
                                    <input type="text" class="form-control" name="asin" id="asin" value="{{ old('asin') }}">
                                  
                                    <button type="submit" class="btn btn-primary float-right mt-3">Submit</button>
                                    </form>
                                </div>
                            </div>
                    
                            <div class="mb-3 row" id="collapse-brand" style="display: none;">
                                <label for="brand" class="col-sm-2 col-form-label">Brand</label>
                                <div class="col-sm-4">
                                    <form action="{{route('vero.addbrandvero')}}" method="post">
                                        @csrf
                                       
                                    <input type="text" class="form-control" name="brand" id="brand">
                                   
                                    <button type="submit" class="btn btn-primary float-right mt-3">Submit</button>
                                </form>
                                </div>
                            </div>
                            
                             <form action="{{route('vero.addbrandvero')}}" method="post">
                                        @csrf
                                       
                                    <input type="text" class="form-control" name="brand" id="brand">
                                   
                                    <button type="submit" id="submit" class="btn btn-primary float-right mt-3">Submit</button>
                                </form>
                    
                            <div class="mb-3 row" id="collapse-judul" style="display: none;"> 
                                <label for="judul" class="col-sm-2 col-form-label">Judul</label>
                                <div class="col-sm-8">
                                    <form action="{{route('vero.addjudulvero')}}" method="post">
                                        @csrf
                                    <input type="text" class="form-control" name="judul" id="judul">
                                   
                                    <button type="submit" class="btn btn-primary float-right mt-3">Submit</button>
                                </form>
                                </div>
                            </div>
                    
                            <div class="mb-3 row" id="collapse-keyword" style="display: none;">
                                <label for="keyword" class="col-sm-2 col-form-label">Keyword</label>
                                <div class="col-sm-4">
                                    <form action="{{route('vero.addKeywordvero')}}" method="post">
                                         @csrf
                                       
                                    <input type="text" class="form-control" name="keyword" id="keyword">
                                   
                                    <button type="submit" class="btn btn-primary float-right mt-3">Submit</button>
                                </form>
                                </div>
                            </div>
                    
                           
                    </div>
                </div>
            </div>
            @include('product.vero.script')
                <!-- /.container-fluid -->

@endsection