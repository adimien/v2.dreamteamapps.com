<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous">
</script>
<script src="https://code.jquery.com/jquery-3.6.1.js" integrity="sha256-3zlB5s2uwoUzrXK3BT7AX3FyvojsraNFxCc2vC/7pNI="
    crossorigin="anonymous"></script>
<script src="//cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
{{-- checkbox --}}
<link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.12/css/dataTables.checkboxes.css" rel="stylesheet" />
<script type="text/javascript" src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.12/js/dataTables.checkboxes.min.js"></script>
<script>
    $(document).ready(function() {
            $('#VeroTable').DataTable({
                processing: true,
                serverside: true,
                ajax:{
                    url: "{{ url('vero') }}"
                    },
                columns: [
                    {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    order: [0, "asc"],
                    orderable: true,
                    searchable: false
                },{
                    data: 'action',
                    name: 'ASIN'
                },{
                    data: 'brand',
                    name: 'Brand'
                },{
                    data: 'judul',
                    name: 'Judul'
                },{
                    data: 'keyword',
                    name: 'Kata /Keyword'
                },{
                    data: 'penginput',
                    name: 'CREATED BY'
                },{
                    data: 'date',
                    name: 'DATE INPUT'
                },{
                    data: 'aksi',
                    name: 'Aksi'
                }]
            });
    });
   
    // GLOBAL SETUP 
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

  
    $('#vero-type').on('change', function() {
        var selectedValue = $(this).val();
        $('#collapse-asin, #collapse-brand, #collapse-judul, #collapse-keyword').hide();
        if (selectedValue === 'asin') {
            $('#collapse-asin').show();
        } else if (selectedValue === 'brand') {
            $('#collapse-brand').show();
        } else if (selectedValue === 'judul') {
            $('#collapse-judul').show();
        } else if (selectedValue === 'keyword') {
            $('#collapse-keyword').show();
        }
    });
   
  $('#VeroTable').on('click','.delete',function(){
    var id = $(this).data('id');
    var deleteConfirm = confirm("Are you sure?");
    if (deleteConfirm == true) {
         // AJAX request
         $.ajax({
             url: "{{ route('vero.destroy') }}",
             type: 'post',
             headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
             data: {id: id},
             success: function(response){
                  if(response.success == 1){
                       alert("Record deleted.");

                       // Reload DataTable
                       VeroTable.ajax.reload();
                  }else{
                        alert("Invalid ID.");
                  }
             }
         });
    }
});


</script>