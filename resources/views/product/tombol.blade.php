
<a href='{{route('barang.edit',$data->id)}}' class="btn btn-warning btn-icon-split tombol-edit">
    <span class="icon text-white-5">
        <i class="fas fa-edit"></i>
    </span>
    <span class="text">Edit</span>
</a>
<a href='#' data-id="{{ $data->id }}" class="delete btn btn-danger btn-icon-split tombol-del"> 
<span class="icon text-white-5">
    <i class="fas fa-trash"></i>
</span>
<span class="text">Hapus</span></a>
