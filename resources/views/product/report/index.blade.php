@extends('layout.template')
<!-- End of Topbar -->
@section('isi')
               
               <!-- Begin Page Content -->
               <div class="container-fluid">

                <!-- Page Heading -->
                <h1 class="h3 mb-2 text-gray-800">Report Data</h1>
                 <p class="mb-4">Pengumuman :<br>Jika mencari data hari ini harus mengisi kolom "to" dengan tanggal besuk.</p>
                
                <div class="card shadow mb-4">
                    
                    <div class="card-body">
                        <div class="mb-3 row">
                            <label for="brand" class="col-sm-2 col-form-label">Pilih data Report</label>
                            <div class="col-sm-4 ml-3">
                                <select name="report-type" id="report-type" class="form-control">
                                    <option value="">Select Type Report</option>
                                    <option value="input" id="input">Total Input Barang</option>
                                    <option value="listing" id="listing">Total Listing Per Akun</option>
                                    <option value="lister" id="lister">Total Listing Per Lister</option>
                                </select>
                            </div>
                            <a href='{{url('report')}}' class="btn btn-warning">Reset</a>
                        </div>
                      
                         Total Input Barang: {{ $count_input }} item<br>
                         Total Listing hari ini : {{$count_listed}} item<br>
                         Total Item belum di listing : {{$count_ready}} item
                        
                        <div class="card-body" id="collapse-input" style="display: none;" >
                            <form action="{{route('report.countBarang')}}" method="get">
                                <div class="mb-3 row">
                                    <label for="brand" class="col-sm-2 col-form-label">Pilih Nama Lister</label>
                                    <div class="col-sm-4">
                                        <select name="penginput" id="penginput" class="form-control">
                                            <option value="">Select Name Lister</option>
                                            @foreach ( $lister as $p)
                                            <option value="{{$p->name}}" id="{{$p->name}}">{{$p->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="mb-3 row">
                                    <label for="brand" class="col-sm-2 col-form-label">Tanggal</label>
                                    <div class="col-sm-4">
                                        <input type="date" id="startDate" name="startDate" class="form-control">
                                    </div>
                                    <label for="brand" class="col-form-label">To</label>
                                    <div class="col-sm-4">
                                        <input type="date" id="endDate" name="endDate" class="form-control">
                                        <button type="submit" class="btn btn-primary float-right mt-3">Submit</button>
                                    </div>
                                    
                                </div>
                            </form>
                        </div>
                               
                        <div class="card-body" id="collapse-listing" style="display: none;" >
                            <form action="{{route('report.countlisting')}}" method="get">
                                <div class="mb-3 row">
                                    <label for="brand" class="col-sm-2 col-form-label">Pilih Nama Akun</label>
                                    <div class="col-sm-4">
                                        <select name="account_ebay" id="account_ebay" class="form-control">
                                            <option value="">Select Account</option>
                                            @foreach ($account_list as $p)
                                            <option value="{{$p->account_id}}" id="{{$p->account_id}}">{{$p->account_id}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="mb-3 row">
                                    <label for="brand" class="col-sm-2 col-form-label">Tanggal</label>
                                    <div class="col-sm-4">
                                        <input type="date" id="startDate" name="startDate" class="form-control">
                                    </div>
                                    <label for="brand" class="col-form-label">To</label>
                                    <div class="col-sm-4">
                                        <input type="date" id="endDate" name="endDate" class="form-control">
                                        <button type="submit" class="btn btn-primary float-right mt-3">Submit</button>
                                    </div>
                                    
                                </div>
                            </form>
                        </div>
                        <!--<table class="table-bordered">-->
                        <!--    <thead>-->
                        <!--    '<tr>-->
                        <!--        <th>Lister</th>-->
                        <!--        <th>Input Today</th>-->
                        <!--    </tr>-->
                        <!--    </thead>-->
                        <!--    <tbody>-->
                        <!--       @foreach ($username as $u)-->
                        <!--            <tr>-->
                        <!--                <td>{{ $u->name }}</td>-->
                        <!--                <td>{{$penginput_today}}</td>-->
                        <!--            </tr>-->
                        <!--        @endforeach-->
                        <!--    </tbody>-->
                            
                        <!--</table>-->
                      
                                @if(isset($count) && $count == 0)
                                    <div class="alert alert-danger">
                                        <h3>Report Total Input Barang :</h3>
                                        <p>Nama : {{ $penginput }} <br>Tanggal Input Barang :  {{\Carbon\Carbon::parse( $startDate )->format('d-M-Y')}} s/d {{\Carbon\Carbon::parse( $endDate )->format('d-M-Y')}} <br> Jumlah Barang : 0 Barang</p>
                                    </div>
                
                                @elseif(isset($count))
                                <div class="alert alert-success">
                                    <h3 class="mb-2">Report Total Input Barang :</h3>
                                    <p>Nama : {{ $penginput }} <br>Tanggal Input Barang :  {{\Carbon\Carbon::parse( $startDate )->format('d-M-Y')}} s/d {{\Carbon\Carbon::parse( $endDate )->format('d-M-Y')}} <br> Jumlah Barang : {{$count}} Barang</p>
                                </div>
                                @endif
                                </p>
                                {{-- result listing per akun --}}
                                @if(isset($count_listing) && $count_listing == 0)
                                <div class="alert alert-danger">
                                    <h3>Report Total Listing Barang :</h3>
                                    <p>Nama : {{ $account_ebay }} <br>Tanggal Listing Barang :  {{\Carbon\Carbon::parse( $startDate )->format('d-M-Y')}} s/d {{\Carbon\Carbon::parse( $endDate )->format('d-M-Y')}} <br> Jumlah Barang : 0 Barang</p>
                                </div>
            
                                @elseif(isset($count_listing))
                                <div class="alert alert-primary">
                                    <h3 class="mb-2">Report Total Listing Barang :</h3>
                                    <p>Nama : {{ $account_ebay }} <br>Tanggal Listing Barang :  {{\Carbon\Carbon::parse( $startDate )->format('d-M-Y')}} s/d {{\Carbon\Carbon::parse( $endDate )->format('d-M-Y')}} <br> Jumlah Barang : {{$count_listing}} Barang</p>
                                </div>
                                @endif
                            </p>
                            
                </div>
            @include('product.report.script')
                <!-- /.container-fluid -->

@endsection