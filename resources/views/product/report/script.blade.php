<script>
     $('#report-type').on('change', function() {
        var selectedValue = $(this).val();
        $('#collapse-input, #collapse-listing, #collapse-lister').hide();
        if (selectedValue === 'input') {
            $('#collapse-input').show();
        } else if (selectedValue === 'listing') {
            $('#collapse-listing').show();
        } else if (selectedValue === 'lister') {
            $('#collapse-lister').show();
        } 
    });
</script>