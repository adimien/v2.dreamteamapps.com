<div class="modal-content">
    <form id="form-account" action="{{route('accounts.update',$data->id)}}" method="post">
      @csrf
    <div class="modal-header">
      <h5 class="modal-title" id="exampleModalLabel">Update Data Account</h5>
      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
    </div>
    <div class="modal-body">
        
        <input type="text" class="form-control" name="id" id="id" value="{{$data->id}}" hidden>
            <div class="form-group">
              <label for="formGroupExampleInput" class="mt-4">ID Account eBay</label>
              <input type="text" class="form-control" name="account_id" id="account_id" value="{{$data->account_id}}">
              <label for="formGroupExampleInput" class="mt-4">E-mail Account eBay</label>
              <input type="text" class="form-control" name="email" id="email" value="{{$data->email}}">

              <label for="formGroupExampleInput" class="mt-4">Lister / Pemegang <span class="badge badge-primary">Baru</span></label>
              <select name="lister" id="lister" class="form-control">
                <option value="">--Pilih--</option>
                @foreach ( $alldata as $d)
                <option value="{{$d->name}}">{{$d->name}}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <label for="formGroupExampleInput">Pemegang Saat ini</label>
             <input type="text" class="form-control" name="" id="" value="{{$data->lister}}" readonly>
            </div>
     
           
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save</button>
        </div>
  </form>
  </div>