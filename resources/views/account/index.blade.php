@extends('layout.template')
<!-- End of Topbar -->
@section('isi')
               
                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-2 text-gray-800">Data Accounts</h1>
                   <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                            @endif
                            <div class="alert alert-danger d-none"></div>
                           <div class="alert alert-success d-none"></div>   
                           <a href='{{url('accounts/add_accs')}}' class="btn btn-success"> <i class="fas fa-fw fa-podcast"></i> Tambah Data Account eBay</h6></a>                   
                        </div>

                        <div class="card-body">
                        <form  id="fm-listing">
                            <div class="table-responsive">
                                <table class="table table-bordered all-data" id="AccountsTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>User ID eBay</th>
                                            <th>EMAIL eBay</th>
                                            <th>Lister/Pemegang</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                </table>
                                
                            </div>
                        </form>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="modalAccount" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                    </div>
                  </div>


                @include('account.script')
@endsection