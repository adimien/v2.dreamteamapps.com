<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous">
</script>
<script src="https://code.jquery.com/jquery-3.6.1.js" integrity="sha256-3zlB5s2uwoUzrXK3BT7AX3FyvojsraNFxCc2vC/7pNI="
    crossorigin="anonymous"></script>
<script src="//cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
{{-- checkbox --}}
<link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.12/css/dataTables.checkboxes.css" rel="stylesheet" />
<script type="text/javascript" src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.12/js/dataTables.checkboxes.min.js"></script>
<script>
    $(document).ready(function() {
            $('#AccountsTable').DataTable({
                processing: true,
                serverside: true,
                ajax:{
                    url: "{{ url('/account') }}"
                    },
                columns: [
                    {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    order: [0, "asc"],
                    orderable: true,
                    searchable: false
                },{
                    data: 'account_id',
                    name: 'User ID eBay'
                },{
                    data: 'email',
                    name: 'EMAIL eBay'
                },{
                    data: 'lister',
                    name: 'Lister/Pemegang'
                },{
                    data: 'aksi',
                    name: 'Aksi'
                }]
            });
    });
   
    // GLOBAL SETUP 
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    $('#AccountsTable').on('click','.edit',function(){
        let data = $(this).data()
        let id = data.id
        let jenis = data.jenis

        $.ajax({
            method :'get',
            url :'accounts/edit/'+id,
            success:function(res){
                $('#modalAccount .modal-dialog').html(res);
                $('#modalAccount').modal('show')
                store()
            }
            })
        })

        function update(){
            $('#form-accountlister').on('submit',function(e){
                var id = $('#id').val()
                var lister = $('#lister').val()
                var email = $('#email').val()
                var account_id = $('#account_id').val()
                $.ajax({
                        method :'post',
                        url :'account/update/'+id,
                        headers: {
                             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                        data : {
                            
                            id:id,
                            lister:lister,
                            email:email,
                            account_id:account_id
                        },
                        success:function(res){
                            $('#modalVero').modal('hide');
                            if (res.redirect) {
                            window.location.href = res.redirect;
                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            console.log(textStatus, errorThrown);
                        }           
             })
        })
    }
</script>