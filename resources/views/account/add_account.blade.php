@extends('layout.template')
<!-- End of Topbar -->
@section('isi')
               
               <!-- Begin Page Content -->
               <div class="container-fluid">

                <!-- Page Heading -->
                <h1 class="h3 mb-2 text-gray-800">Add User ID eBay</h1>
                
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary"> <a href='{{url('account')}}' class="btn btn-warning">+ Kembali</a></h6>
                    </div>
                    
                    <div class="card-body">
                        <form action="{{ route('accounts.addacc') }}" method="POST">
                            @csrf
                        @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                        @endif
                        <div class="mb-3 row">
                            <label for="account_id" class="col-sm-2 col-form-label">User ID eBay</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name='account_id' id="account_id"  value="{{old('account_id')}}">
                                @error('account_id')
                                <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="email" class="col-sm-2 col-form-label">E-mail eBay</label>
                            <div class="col-sm-6">
                                <input type="email" class="form-control" name='email' id="email"  value="{{old('email')}}">
                                @error('email')
                                <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="lister" class="col-sm-2 col-form-label">Lister / Pemegang</label>
                            <div class="col-sm-4">
                                <select class="form-control" name="lister" id="lister">
                                    @foreach ($lister as $user)
                                        <option  value="{{ $user->name}}">{{ $user->name }}</option>
                                    @endforeach
                                </select>
                               
                                @error('lister')
                                <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary ml-3">Submit</button>
                        </form>
                    </div>
                </div>
            </div>

                <!-- /.container-fluid -->

@endsection