@extends('layout.template')
<!-- End of Topbar -->
@section('isi')
               
                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-2 text-gray-800">Data User</h1>
                   <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                            @endif
                            <div class="alert alert-danger d-none"></div>
                           <div class="alert alert-success d-none"></div>   
                           <a href='{{url('user/register')}}' class="btn btn-danger"> <i class="fas fa-fw fa-users"></i> Tambah User</h6></a>                   
                        </div>

                        <div class="card-body">
                        <form  id="fm-listing">
                            <div class="table-responsive">
                                <table class="table table-bordered all-data" id="userTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama User</th>
                                            <th>E-mail</th>
                                            <th>Role</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                </table>
                                
                            </div>
                        </form>
                        </div>
                    </div>
                </div>


                @include('user.script')
@endsection