@extends('layout.template')
<!-- End of Topbar -->
@section('isi')
               
               <!-- Begin Page Content -->
               <div class="container-fluid">

                <!-- Page Heading -->
                <h1 class="h3 mb-2 text-gray-800">Add User Pegawai</h1>
                
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary"> <a href='{{url('user')}}' class="btn btn-warning">+ Kembali</a></h6>
                    </div>
                    
                    <div class="card-body">
                        <form action="{{ route('user.adduser') }}" method="POST">
                            @csrf
                        @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                        @endif
                        <div class="mb-3 row">
                            <label for="name" class="col-sm-2 col-form-label">Nama</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" name='name' id="name"  value="{{old('name')}}">
                                @error('name')
                                <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="judul" class="col-sm-2 col-form-label">Email</label>
                            <div class="col-sm-8">
                                <input type="email" class="form-control" name='email' id="email">
                                @error('email')
                                <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="harga" class="col-sm-2 col-form-label">Password</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" name='password' id="password">
                                @error('password')
                                <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="role" class="col-sm-2 col-form-label">Role</label>
                            <div class="col-sm-8">
                                <select name="role" id="role" class="form-control col-sm-2 ">
                                    <option value="0">Super Admin</option>
                                    <option value="1">Admin</option>
                                    <option value="2">Lister</option>
                                    <option value="3">Riset</option>
                                </select>
                                @error('role')
                                <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary ml-3">Submit</button>
                        </form>
                    </div>
                </div>
            </div>

                <!-- /.container-fluid -->

@endsection