<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous">
</script>
<script src="https://code.jquery.com/jquery-3.6.1.js" integrity="sha256-3zlB5s2uwoUzrXK3BT7AX3FyvojsraNFxCc2vC/7pNI="
    crossorigin="anonymous"></script>
<script src="//cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
{{-- checkbox --}}
<link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.12/css/dataTables.checkboxes.css" rel="stylesheet" />
<script type="text/javascript" src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.12/js/dataTables.checkboxes.min.js"></script>
<script>
    $(document).ready(function() {
            $('#userTable').DataTable({
                processing: true,
                serverside: true,
                ajax:{
                    url: "{{ url('user') }}"
                    },
                columns: [
                    {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    order: [0, "asc"],
                    orderable: true,
                    searchable: false
                },{
                    data: 'name',
                    name: 'Nama'
                },{
                    data: 'email',
                    name: 'Email'
                },{
                    data: 'role',
                    name: 'Role'
                },{
                    data: 'aksi',
                    name: 'Aksi'
                }]
            });
    });
   
    // GLOBAL SETUP 
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

  //DELETE
  $(document).on('click', '.delete', function(){
      product_id = $(this).attr('id');
      $('#confirmModal').modal('show');
  });

  $('#ok_button').click(function(){
      $.ajax({
          url:"product/destroy/"+product_id,
          beforeSend:function(){
              $('#ok_button').text('Deleting...');
          },
          success:function(data)
          {
              setTimeout(function(){
              $('#confirmModal').modal('hide');
              $('#dataTable').DataTable().ajax.reload();
              alert('Data Deleted');
              }, 2000);
          }
      })
  });

  $(document).on('click', '#bulk_delete', function(){
      var id = [];
      if(confirm("Are you sure you want to Delete this data?"))
      {
          $('.product_checkbox:checked').each(function(){
              id.push($(this).val());
          });
          if(id.length > 0)
          {
              $.ajax({
                  url:"{{ route('product.removeall')}}",
                  headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                  method:"get",
                  data:{id:id},
                  success:function(data)
                  {
                      console.log(data);
                      alert(data);
                      window.location.assign("product"); 
                  },
                  error: function(data) {
                      var errors = data.responseJSON;
                      console.log(errors);
                  }
              });
          }
          else
          {
              alert("Please select atleast one checkbox");
          }
      }
  });
  
      
//Checkbox
    $("#hd-cb").on('click', function(){
        var isChecked =  $("#hd-cb").prop('checked')
            $(".product_checkbox").prop('checked',isChecked)
            $("#btn-list-all").prop('disabled',!isChecked)
            $("#bulk_delete").prop('disabled',!isChecked)
    })
    $("#dataTable").on('click', '.product_checkbox',function(){
        if($(this).prop('checked')!==true){
            $("#hd-cb").prop('checked',false)
        }
        let semua_checked = $("#dataTable .product_checkbox:checked")
        let button_list_status = (semua_checked.length>0)
        $("#btn-list-all").prop('disabled',!button_list_status)
        $("#bulk_delete").prop('disabled',!button_list_status)
    })

//update
// Handle form submission event
$(document).on('click', '#btn_update', function(){
      var id = [];
      var listingBanyak = $('#listing-banyak').val();
      if(confirm("Are you sure you want to List this data?"))
      {
          $('.product_checkbox:checked').each(function(){
              id.push($(this).val());
          });
          if(id.length > 0)
          {
              $.ajax({
                  url:"{{ route('barang.listing')}}",
                  headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                  method:"post",
                  data:{
                    id:id,
                    account_ebay:listingBanyak
                },
                  success:function(data)
                {
                    if (data.errors) {
                    $('.alert-danger').removeClass('d-none');
                    $('.alert-danger').html("<ul>");
                    $.each(data.errors, function(key, value) {
                        $('.alert-danger').find('ul').append("<li>" + value +
                            "</li>");
                    });
                    $('.alert-danger').append("</ul>");
                    } else {
                       
                        $('.alert-success').removeClass('d-none');
                        $('.alert-success').html(data.success);
                    }
                    
                    $('#dataTable').DataTable().ajax.reload();
                    
                }
              });
          }
          else
          {
              alert("Please select atleast one checkbox");
          }
      }
  });

</script>