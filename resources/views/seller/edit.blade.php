<div class="modal-content">
  <form id="form-action" action="{{route('seller.update',$data_seller->id)}}" method="post">
    @csrf
  <div class="modal-header">
    <h5 class="modal-title" id="exampleModalLabel">User ID Seller : {{$data_seller->seller_ebay}}</h5>
    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
  </div>
  <div class="modal-body">
      
          <div class="form-group">
            <label for="formGroupExampleInput">Apakah Anda Ingin Menggunakan Seller ini ?</label>
            <select name="status_seller" id="status_seller" class="form-control">
              <option value="{{$data_seller->status_seller}}">{{$data_seller->status_seller}}</option>
              <option value="Pakai">Pakai</option>
              <option value="Tidak Pakai">Tidak Pakai</option>
            </select>
          </div>
          <div class="form-group">
            <label for="formGroupExampleInput">Keterangan Seller</label>
           <input type="text" class="form-control" name="ket_seller" id="ket_seller" value="{{$data_seller->ket_seller}}">
           <input type="text" class="form-control" name="penginput_seller" id="penginput_seller" value="{{auth()->user()->name}}" hidden>
          </div>
         
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
</form>
</div>