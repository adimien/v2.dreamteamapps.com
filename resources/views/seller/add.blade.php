@extends('layout.template')
<!-- End of Topbar -->
@section('isi')
               
               <!-- Begin Page Content -->
               <div class="container-fluid">

                <!-- Page Heading -->
                <h1 class="h3 mb-2 text-gray-800">Add Seller Competitor</h1>
                
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary"> <a href='{{url('seller')}}' class="btn btn-warning">+ Kembali</a></h6>
                    </div>
                    
                    <div class="card-body">
                        <form action="{{ url('/seller/addseller') }}" method="POST">
                            @csrf
                            @if ($message = Session::get('success'))
                                <div class="alert alert-success">
                                    <p>{{ $message }}</p>
                                </div>
                            @endif
                            <div class="mb-3 row">
                                <label for="seller_ebay" class="col-sm-2 col-form-label">User ID Competitor</label>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control" name="seller_ebay" id="seller_ebay" value="{{ old('seller_ebay') }}">
                                    @error('seller_ebay')
                                        <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="mb-3 row" hidden>
                                <label for="status_seller" class="col-sm-2 col-form-label">Status seller</label>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control" name="status_seller" id="status_seller" value="publish">
                                    @error('status_seller')
                                        <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="mb-3 row" hidden>
                                <label for="ket_seller" class="col-sm-2 col-form-label">Keterangan</label>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control" name="ket_seller" id="ket_seller" value="publish">
                                    @error('ket_seller')
                                        <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label for="penginput_seller" class="col-sm-2 col-form-label">Penginput</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="penginput_seller" id="penginput_seller" value="{{ Auth::user()->name }}" readonly>
                                    @error('penginput_seller')
                                        <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary ml-3">Submit</button>
                        </form>
                    </div>
                </div>
            </div>

                <!-- /.container-fluid -->

@endsection