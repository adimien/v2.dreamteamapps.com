<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous">
</script>
<script src="https://code.jquery.com/jquery-3.6.1.js" integrity="sha256-3zlB5s2uwoUzrXK3BT7AX3FyvojsraNFxCc2vC/7pNI="
    crossorigin="anonymous"></script>
<script src="//cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
{{-- checkbox --}}
<link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.12/css/dataTables.checkboxes.css" rel="stylesheet" />
<script type="text/javascript" src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.12/js/dataTables.checkboxes.min.js"></script>
<script>
    $(document).ready(function() {
            $('#sellerTable').DataTable({
                processing: true,
                serverside: true,
                ajax:{
                    url: "{{ route('seller.list_seller') }}"
                    },
                columns: [
                    {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    order: [0, "asc"],
                    orderable: true,
                    searchable: false
                },{
                    data: 'seller',
                    name: 'USER ID SELLER'
                },
                {
                    data: 'last_used',
                    name: 'Terakhir Pakai'
                },{
                    data: 'bisa_pakai',
                    name: 'Waktu Bisa Pakai'
                },
                {
                    data: 'penginput_seller',
                    name: 'Lister'
                },{
                    data: 'status',
                    name: 'status_seller'
                },{
                    data: 'ket_seller',
                    name: 'Keterangan'
                },{
                    data: 'aksi',
                    name: 'Keterangan'
                }]
            });
    });
   
    // GLOBAL SETUP 
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('#sellerTable').on('click','.action', function(){
        let data = $(this).data()
        let id = data.id
        let jenis = data.jenis

        $.ajax({
            method :'get',
            url :'seller/edit/'+id,
            success:function(res){
                $('#modalAction .modal-dialog').html(res);
                $('#modalAction').modal('show')
                store()
            }
        })

        function store(){
            $('#form-action').on('submit',function(e){
                var status_seller = $('#status_seller').val()
                var ket_seller = $('#ket_seller').val()
                var penginput_seller = $('#penginput_seller').val()
                $.ajax({
                        method :'post',
                        url :'seller/update/'+id,
                        headers: {
                             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                        data : {
                            
                            status_seller:status_seller,
                            ket_seller:ket_seller,
                            penginput_seller:penginput_seller
                        },
                        success:function(res){
                            $('#modalAction').modal('hide');
                            if (res.redirect) {
                            window.location.href = res.redirect;
                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            console.log(textStatus, errorThrown);
                        }           
        })
            })
        }
       
    })





  //DELETE
  $(document).on('click', '.delete', function(){
      product_id = $(this).attr('id');
      $('#confirmModal').modal('show');
  });

  $('#ok_button').click(function(){
      $.ajax({
          url:"product/destroy/"+product_id,
          beforeSend:function(){
              $('#ok_button').text('Deleting...');
          },
          success:function(data)
          {
              setTimeout(function(){
              $('#confirmModal').modal('hide');
              $('#dataTable').DataTable().ajax.reload();
              alert('Data Deleted');
              }, 2000);
          }
      })
  });

  $(document).on('click', '#bulk_delete', function(){
      var id = [];
      if(confirm("Are you sure you want to Delete this data?"))
      {
          $('.product_checkbox:checked').each(function(){
              id.push($(this).val());
          });
          if(id.length > 0)
          {
              $.ajax({
                  url:"{{ route('product.removeall')}}",
                  headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                  method:"get",
                  data:{id:id},
                  success:function(data)
                  {
                      console.log(data);
                      alert(data);
                      window.location.assign("product"); 
                  },
                  error: function(data) {
                      var errors = data.responseJSON;
                      console.log(errors);
                  }
              });
          }
          else
          {
              alert("Please select atleast one checkbox");
          }
      }
  });
  
      
//Checkbox
    $("#hd-cb").on('click', function(){
        var isChecked =  $("#hd-cb").prop('checked')
            $(".product_checkbox").prop('checked',isChecked)
            $("#btn-list-all").prop('disabled',!isChecked)
            $("#bulk_delete").prop('disabled',!isChecked)
    })
    $("#dataTable").on('click', '.product_checkbox',function(){
        if($(this).prop('checked')!==true){
            $("#hd-cb").prop('checked',false)
        }
        let semua_checked = $("#dataTable .product_checkbox:checked")
        let button_list_status = (semua_checked.length>0)
        $("#btn-list-all").prop('disabled',!button_list_status)
        $("#bulk_delete").prop('disabled',!button_list_status)
    })

//update
// Handle form submission event
 // get Edit Product
 $('.btn-update').on('click',function(){
            
        });
 



</script>