@extends('layout.template')
<!-- End of Topbar -->
@section('isi')
               
                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-2 text-gray-800">Data Seller</h1>
                   <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                            @endif
                            <div class="alert alert-danger d-none"></div>
                           <div class="alert alert-success d-none"></div>   
                           <a href='{{url('/seller/add_seller')}}' class="btn btn-warning"> <i class="fas fa-fw fa-compass"></i> Tambah Seller</h6></a>                   
                        </div>

                        <div class="card-body">
                        <form  id="fm-listing">
                            <div class="table-responsive">
                                <table class="table table-bordered all-data" id="sellerTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>USER ID SELLER</th>
                                            <th>Terakhir Pakai</th>
                                            <th>Waktu Bisa Pakai</th>
                                            <th>Lister</th>
                                            <th>status_seller</th>
                                            <th>Keterangan</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                </table>
                                
                            </div>
                        </form>
                        </div>
                    </div>
                </div>
<!-- Modal -->
<!-- HTML code for the modal -->
<div class="modal fade" id="modalAction" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
    </div>
  </div>
  

                @include('seller.script')
@endsection