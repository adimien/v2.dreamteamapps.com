<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CreateUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
        [
            'name'=>'Superadmin',
            'email'=>'Superadmin@gmail.com',
            'role'=> 0,
            'password'=> bcrypt('123456'),
         ],
         [
            'name'=>'admin',
            'email'=>'admin@gmail.com',
            'role'=> 1,
            'password'=> bcrypt('123456'),
         ],
         [
            'name'=>'lister',
            'email'=>'lister@gmail.com',
            'role'=> 2,
            'password'=> bcrypt('123456'),
         ],
         [
            'name'=>'riset',
            'email'=>'riset@gmail.com',
            'role'=> 3,
            'password'=> bcrypt('123456'),
         ],
        ];
         foreach ($users as $key => $user) 
        {
            User::create($user);
        }
    }
}
